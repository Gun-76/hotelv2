package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;

public class Hotel {

	Scanner in = new Scanner(System.in);

	private String nom = "CALIFORNIA";
	private Chambre[] chambre;
	private Employer employer;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom_) {
		nom = nom_;
	}

	public Chambre[] getChambre() {
		return chambre;
	}

	public void setChambre(Chambre[] chambre_) {
		chambre = chambre_;
	}

	public Employer getEmployer() {
		return employer;
	}

	public void setEmployer(Employer employer_) {
		employer = employer_;
	}

	public Hotel() throws InterruptedException {
		creeHotel();
		Scanner in=new Scanner(System.in);
		demarrer(in);
	}

	@Override
	public String toString() {
		return "Hotel [nom=" + nom + ", chambre=" + Arrays.toString(chambre) + ", employer=" + employer + "]";
	}

	/**
	 * Fonction qui nous retourne le nombre de chambre
	 * 
	 * @return le nombre de chambres
	 */
	public int nbChambre() {
		int nbChambre = 0;

		String[] lignes = Control.caractChambre();
		for (int i = 1; i < lignes.length; i++) {
			String ligne[] = Control.getLigne(i);
			nbChambre += Integer.parseInt(ligne[5]);
		}
		return nbChambre;
	}

	/**
	 * fonction permettant d'afficher l'espace login
	 * @param employe
	 * @param client
	 * @throws InterruptedException 
	 */	
	public void afficheLogin(Employer employe, Scanner in) throws InterruptedException {
		System.out.println("etes vous un employe ou un client ?");

		System.out.println("1- Employer");
		System.out.println("2- Client");
		int choix = in.nextInt();
		in.nextLine();
		boolean connectEmploye = false;
		boolean connectClient = false;	
		if(choix == 1) {		 
			while(!connectEmploye){
				connectEmploye = employe.seConnecter();
			}
			if(connectEmploye) {
				demmarerMenu(in);
			}
		}
		else if(choix ==2) {
			String login_="";
			boolean trouveClient = false;
			for (int i = 0; i < chambre.length; i++) {
					
						for (int j = 0; j < chambre[i].getComptRes(); j++) {
						while(!connectClient){
							connectClient =  chambre[i].getResChamb()[j].getClient().seConnecter(login_);
							login_ = chambre[i].getResChamb()[j].getClient().getLogin();
							
						}
						if(connectClient) {	
							trouveClient = true;
							for(int g = 0; g< chambre.length; g++) {	
							for (int k = 0; k < chambre[g].getComptRes(); k++) {
								if(chambre[g].getResChamb()[k].getClient().getLogin().equals(login_)) {
									chambre[g].getResChamb()[k].detailReservation(chambre[g]);
								}					
							}
							}
							
						}
				
			}
			}
			if(trouveClient) {
				Thread.sleep(20000);
				afficheLogin(employe, in);
			}
			if(!trouveClient) {
				System.out.println("Pas de client pour l'instant");
				afficheLogin(employe, in);
			}
			}
	}
/**
 * Affichage du menu de l'hotel
 */
	public void affichageMenu() {
		System.out.println(
				"-------------------------------MENU HOTEL CDA JAVA--------------------------------------------------------------------\n"
						+ "A-Afficher l'etat de l'hotel \n" + "B-Afficher le nombre de chambres reservees\n"
						+ "C-Afficher le nombre de chambres libres\n"
						+ "D-Afficher le numero de la premiere chambre vide\n"
						+ "E-Afficher le numero de la derniere chambre vide\n" + "F-Reserver une chambre \n"
						+ "G-Liberer une chambre \n" + "H-Modifier une reservation\n" + "I-Annuler une reservation\n"
						+ "J-Chiffre d'affaire de l'hotel\n" + "Q-Quitter\n"
						+ " ---------------------------------------------------------------------------------------------------------------------\n");
	}

	/**
	 * Fonction qui demarre le programme
	 * 
	 * @param hotel notre hotel nous avons besoin de ces fonctions car elles servent
	 *              pour la fonction choixMenu
	 * @param in    le scanner nous avons besoin de ces fonctions car elles servent
	 *              pour la fonction choixMenu
	 * @throws InterruptedException 
	 */
	public void demmarerMenu(Scanner in) throws InterruptedException {
		affichageMenu();
		while (choixMenu(in) == true)
			affichageMenu();
	}

	/**
	 * Fonction qui gere le choix du premier menu
	 * 
	 * @param in en entrer le scanner
	 * @return un boolean qui gere l'affichage du menu1
	 * @throws InterruptedException 
	 */
	public boolean choixMenu(Scanner in) throws InterruptedException {// voir aussi les parametre d'entrer qu'il nous faudra
		String choix = in.nextLine();
		switch (choix.toUpperCase()) {// le choix peut etre une majuscule ou une miniscule avec le toUpperCase
		case "A":
			etatHotel();
			break;
		case "B"://!!!!!revoir l'affichage
			System.out.println("Actuellement il y a " + nbChambRes() + " chambre(s) reservee(s)");
			break;
		case "C":
			System.out.println("Actuellement il y a " + nbChambVide() + " Chambre(s) libre(s)");
			break;
		case "D":
			System.out.println("La chambre " + num1ChambVide() + " est la premiere chambre libre");
			break;
		case "E":
			System.out.println("La chambre " + numDernChambVide() + " est la derniere chambre vide");
			break;
		case "F":
			affichageType(in);
			break;
		case "G":
			// liberer une chambre
			libererChambre(chambre);
			
			break;
		case "H":
			// modifier une reservation
			modifierReservation(chambre);
			break;
		case "I":
			// annuler une reservation
			annulerReservation(chambre);
			break;
		case "J":
			// afficher le chiffre d'affaire a une date donner
			break;
		case "Q":
			afficheLogin(employer, in);
		default:
			System.out.println("Votre choix est incorrect");
			break;
		}
		return true;
	}

	/**
	 * Fonction qui retourne l'affichage de l'etat de l'hotel
	 * 
	 * @param hotel
	 */
	public void etatHotel() {
		for (int i = 0; i < getChambre().length; i++) {
			for (int j = 0; j < getChambre()[i].getComptRes(); j++) {
				if(getChambre()[i].getResChamb()[j]!=null) {
				getChambre()[i].getResChamb()[j].detailReservation(getChambre()[i]);
				}
			}
			
		}
	}

	/**
	 * Fonction qui retourne le nombre de chambre reservee
	 * 
	 * @param hotel en entree l'hotel ou on viens comtabiliser le comteur de
	 *              reservation
	 * @return le nombre de chambres reservees
	 */
	public int nbChambRes() {
		int nbChambRes = 0;
		for (int i = 0; i < getChambre().length; i++) {
			if (getChambre()[i].istOccup() == true) {
				nbChambRes += 1;
			}
		}
		return nbChambRes;
	}

	/**
	 * Fonction qui retourne le nombre de chambre vide
	 * 
	 * @param hotel en entree l'hotel ou l'on viens comtabiliser le nombre de
	 *              chambre vide
	 * @return le nombre de chambre vide
	 */
	public int nbChambVide() {
		int nbChambVide = 0;
		for (int i = 0; i < getChambre().length; i++) {
			if (getChambre()[i].istOccup() == false) {
				nbChambVide += 1;
			}
		}
		return nbChambVide;
	}

	/**
	 * Fonction qui retourne le numero de la premier chambre vide
	 * 
	 * @param hotel en entree l'hotel ou l'on viens chercher la premiere chambre
	 *              vide
	 * @return le numero de la premiere chambre vide
	 */
	public int num1ChambVide() {
		int num1ChambVide = 0;
		for (int i = 0; i < getChambre().length; i++) {
			if (getChambre()[i].istOccup() == false) {
				num1ChambVide = getChambre()[i].getNumChambre();
				break;
			}
		}
		return num1ChambVide;
	}

	/**
	 * Fonction qui retourne le numero de la derniere chambre vide
	 * 
	 * @param hotel en entree l'hotel ou on viens chercher la derniere chambre vide
	 * @return le numero de la derniere chambre vide
	 */
	public int numDernChambVide() {
		int numDernChambVide = 0;
		for (int i = nbChambre() - 1; i >= 0; i--) {
			if (getChambre()[i].istOccup() == false) {
				numDernChambVide = getChambre()[i].getNumChambre();
				break;
			}
		}
		return numDernChambVide;
	}

	/**
	 * fonction qui cree l'hotel creation du tableau de chambre de l'hotel
	 */
	public void creeHotel() {
		chambre = new Chambre[nbChambre()];
		int k = 0;
		while (k < nbChambre()) {
			String[] lignes = Control.caractChambre();
			for (int i = 1; i < lignes.length; i++) {
				int nbType = 0;
				String ligne[] = Control.getLigne(i);
				nbType = Integer.parseInt(ligne[5]);
				for (int j = 0; j < nbType; j++) {
					Chambre chamb = new Chambre(i, k + 1);
					chambre[k] = chamb;
					k++;
				}
			}
		}
	}

	/**
	 * Fonction qui demarre le programme
	 * 
	 * @param hotel
	 * @param in
	 * @throws InterruptedException 
	 */
	public void demarrer(Scanner in) throws InterruptedException {
		employer = new Employer();
		afficheLogin(employer, in);

	}
	/**
	 * fonction qui retourne une carateristique de type de chambre 
	 * @param recherche	le numero du parametre recrherche
	 * @return	la caracteristique recherche
	 */
	public  String getType(int recherche) {
	String[] lignes = Control.caractChambre();
	String caracteristique = "";
	for (int i = 1; i < lignes.length; i++) {
		String ligne[] = Control.getLigne(i);
		caracteristique=ligne[recherche];
	}
	return caracteristique;
}
	/**
	 * fonction qui nous permet de selectionner le type de la chambre a reserver
	 * @param in le choix du type de chambre
	 */
	public  void affichageType(Scanner in) {
		System.out.println("Quelle type de chambre voulez vous reserver ?");
		String[] lignes = Control.caractChambre();
		for (int i = 1; i < lignes.length; i++) {
	
			String ligne[] = Control.getLigne(i);
			
			System.out.println(i+"." + ligne[0]+" / Vue: "+ligne[2]+" / Taille: "+ligne[1]+" /Occupation: "+ligne[3]+"\nOption: "+ligne[6]+"\nPrix: "+ligne[4]);
		}
		int choix = in.nextInt();
		in.nextLine();
		Chambre chambreType=null;
		switch (choix) {
		case 1:
			if (isLibre(0,6)) {
				chambreType= chambreLibre(0,6);
			}
			break;
		case 2:
			
			if (isLibre(7,18)){
				chambreType=chambreLibre(7,18);
			}
			break;
		case 3:
			if (isLibre(19,26)) {
				chambreType=chambreLibre(19,26);
			}
			break;
		case 4:
			if (isLibre(27,36)) {
				chambreType=chambreLibre(27,36);
			}
			break;
		case 5:
			if (isLibre(37,48)) {
				chambreType=chambreLibre(37,48);
			}
			break;
		case 6:
			if (isLibre(49,53)) {
				chambreType=chambreLibre(49,53);
			}
			break;
		case 7:
			if (isLibre(54,60)) {
				chambreType=chambreLibre(54,60);
			}
			break;
		case 8:
			if (isLibre(61,64)) {
				chambreType=chambreLibre(61,64);
			}
			break;
		default:
			System.out.println("Votre choix n'est pas correct!");
			break;
		}if(chambreType!=null) {
			chambreType.reserver( in, chambre);
		}
		
		
	}
	/**
	 * fonction qui retourne la premiere chambre libre de son type
	 * @param de	l'indice de depart du type
	 * @param a		l'indice de fin du type
	 * @return	la premiere chambre libre
	 */
	public Chambre chambreLibre(int de, int a) {
		int indiceChambre=0;
		for (int i = de; i <= a; i++) {
			if (!chambre[i].istOccup()) {
				indiceChambre=i;
				System.out.println("La chambre "+chambre[i].getNumChambre()+" est disponible");
					break;
			}
		
		}
		return chambre[indiceChambre];
	}
	/**
	 * Fonction qui retourne si la chambre du type est libre
	 * @param de	debut de la plage du type
	 * @param a		fin de la plage du type
	 * @return		nous retourne l'occupation de la chambre
	 */
	public boolean isLibre(int de, int a) {
		boolean noOccuper = false;
		for (int i = de; i <= a; i++) {
			if (!chambre[i].istOccup()) {
				noOccuper=true;
				break;
			}
		}if(!noOccuper) {
			System.out.println("Pas de chambre disponnible");
		}
		return noOccuper;
	}
	
	
	/**
	 * fonction permettant de modifier les dates de séjour d'une reservation
	 * @param chambre
	 */
	public void modifierReservation(Chambre[] chambre) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("Entrez le numero de reservation :");
		String numReserv = in.nextLine();
		for(int i=0; i<chambre.length;i++) {
			for (int j = 0; j < chambre[i].getComptRes() && chambre[i].getComptRes()!=0; j++) {
				if(chambre[i].getResChamb()[j].getNumReservation().equals(numReserv)) {
					System.out.println("Entrez les nouvelles date :");				
					chambre[i].getResChamb()[j].date(in);
					chambre[i].getResChamb()[j].payer(chambre[i],in);
				}					
			}
		}
	}
	
	/**
	 * fonction permettant de libérer une chambre
	 * @param chambre
	 */
	public void libererChambre(Chambre[] chambre) {
		Scanner in = new Scanner(System.in);
		System.out.println("Quelle chambre doit etre libere ? (numero de chambre)");
		int numChambre_= in.nextInt();
		in.nextLine();
		for(int i=0; i<chambre.length;i++) {
			if(chambre[i].getNumChambre() == numChambre_) {			
				for (int j = 0; j < chambre[i].getComptRes(); j++) {			 
					  chambre[i].setOccup(false);					  
					  System.out.println("La chambre "+ chambre[i].getNumChambre()+" a ete libere");
					  chambre[i].setComptReservation(chambre[i].getComptRes()-1) ;
					  chambre[i].getResChamb()[j]=null;
				  }
				
			}
		}
	}
		
	/**
	 * fonction permettant d'annuler une reservation
	 * @param chambre
	 */
	public void annulerReservation(Chambre[] chambre) {
		Scanner in = new Scanner(System.in);
		System.out.println("Quelle reservation doit etre annule ? (numero de reservation)");
		String numReserv_= in.nextLine();
		for(int i=0; i<chambre.length;i++) {
			for (int j = 0; j < chambre[i].getComptRes(); j++) {
				if(chambre[i].getResChamb()[j].getNumReservation().equals(numReserv_)) {
					chambre[i].setOccup(false);				  
					  System.out.println("La reservation de la chambre "+ chambre[i].getNumChambre()+" a ete annule");
					  chambre[i].getResChamb()[j].remboursement(chambre[i].getTarif());
					  chambre[i].setComptReservation(chambre[i].getComptRes()-1) ;
					  chambre[i].getResChamb()[j]=null;
				}					
			}
		}
	}
	
}