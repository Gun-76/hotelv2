package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Control {
	static Scanner in = new Scanner(System.in);

	/**
	 * Fonction qui retourne un tableau de ligne
	 * 
	 * @return un tableau de ligne de type de chambre
	 */
	public static String[] caractChambre() {
		String[] lignes = new String[9];
		int i = 0;
		FileReader fr = null;
		try {
			fr = new FileReader("C:\\ENV\\Ressources\\ListeChambres_V3.csv");
			BufferedReader br = new BufferedReader(fr);
			try {
				while (br.ready()) {
					lignes[i] = br.readLine();
					i++;
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return lignes;
	}

	/**
	 * fonction qui recupere un tableau de caracteristiques
	 * 
	 * @param indice
	 * @return retourne les caracteristiques d'une chambre
	 */
	public static String[] getLigne(int indice) {
		String[] lignes = caractChambre();
		String[] ligne = lignes[indice].split(";");
		return ligne;
	}
	

}
