package fr.afpa.objets;

import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.Scanner;

public class Chambre {
	Scanner in = new Scanner(System.in);
	private int numChambre;
	private String type;
	private String taille;
	private String vue;
	private String occupation;
	private int tarif;
	private String option;
	private boolean occup;
	private Reservation resChamb[] = new Reservation[5];
	private int comptRes;

	public Reservation[] getResChamb() {
		return resChamb;
	}

	public void setResChamb(Reservation[] resChamb_) {
		resChamb = resChamb_;
	}

	public int getComptRes() {
		return comptRes;
	}

	public void setComptRes() {
		comptRes = comptRes + 1;
	}
	
	public void setComptReservation(int comptreserv) {
		comptRes = comptreserv;
	}
	public int getNumChambre() {
		return numChambre;
	}

	public void setNumChambre(int numChambre_) {
		numChambre = numChambre_;
	}

	public String getType() {
		return type;
	}

	public void setType(String type_) {
		type = type_;
	}

	public String getTaille() {
		return taille;
	}

	public void setTaille(String taille_) {
		taille = taille_;
	}

	public String getVue() {
		return vue;
	}

	public void setVue(String vue_) {
		vue = vue_;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation_) {
		occupation = occupation_;
	}

	public int getTarif() {
		return tarif;
	}

	public void setTarif(int tarif_) {
		tarif = tarif_;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option_) {
		option = option_;
	}

	public boolean istOccup() {
		return occup;
	}

	public void setOccup(boolean occup_) {
		occup = occup_;
	}

	public Chambre(int indice, int numChambre_) {
		getLigne(indice);
		numChambre = numChambre_;
		occup = false;
		comptRes = 0;
	}

	@Override
	public String toString() {
		return "Chambre [numChambre=" + numChambre + ", type=" + type + ", taille=" + taille + ", vue=" + vue
				+ ", occupation=" + occupation + ", tarif=" + tarif + ", option=" + option + ", occup=" + occup
				+ ", resChamb=" + Arrays.toString(resChamb) + ", comptRes=" + comptRes + "]";
	}

	/**
	 * Fonction qui retourne le constructeur des chambre
	 * 
	 * @param indice
	 */
	public void getLigne(int indice) {
		String[] lignes = Control.caractChambre();
		String[] ligne = lignes[indice].split(";");
		numChambre++;
		type = ligne[0];
		taille = ligne[1];
		vue = ligne[2];
		occupation = ligne[3];
		tarif = Integer.parseInt(ligne[4]);
		option = ligne[6];
		occup = false;
	}

	public boolean reserver(Scanner in, Chambre[] cham) {
		System.out.println(
				"Reserver une chambre dans l'hotel California\n1.Creer une client\n2.Rechercher un client\n3.Quitter");
		int choixClient = in.nextInt();
		in.nextLine();
		switch (choixClient) {
		case 1:
			if (getComptRes() < 5) {

				getResChamb()[getComptRes()] = new Reservation(in);
				occup = true;
				getResChamb()[getComptRes()].detailReservation(this);
				getResChamb()[getComptRes()].payer(this, in);
				setComptRes();

			} else {
				System.out.println("Plus de reservation possible pour cette chambre");
			}
			break;
		case 2:
			System.out.println("Entrez un Nom");
			String nomRecherche = in.nextLine();
			boolean clientTrouve = false;
			int iTemp = 0;
			int jTemp = 0;
			for (int i = 0; i < cham.length; i++) {
				for (int j = 0; j <= getComptRes(); j++) {
					if (cham[i].resChamb[j] != null) {
						if (nomRecherche.equals(cham[i].getResChamb()[j].getClient().getNom())) {
							clientTrouve = true;
							iTemp = i;
							jTemp = j;
						}
					}
				}
			}

			if (getComptRes() < 4 && clientTrouve) {
				getResChamb()[getComptRes()] = new Reservation(cham[iTemp].getResChamb()[jTemp].getClient(), in);
				occup = true;
				getResChamb()[getComptRes()].detailReservation(this);
				getResChamb()[getComptRes()].payer(this, in);
				setComptRes();
			} else {
				System.out.println("Plus de reservation possible pour cette chambre");
			}

			if (!clientTrouve) {
				System.out.println("Le client n'existe pas");
			}
			break;
		case 3:
			return false;
		default:
			System.out.println("Votre choix est incorrect");
			break;
		}
		return true;
	}
	
	

}
