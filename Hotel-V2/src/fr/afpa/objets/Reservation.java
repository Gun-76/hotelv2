package fr.afpa.objets;

import java.awt.image.RescaleOp;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

public class Reservation {

	String numReservation ="";
	private Client client;
	private LocalDate dateEnt;
	private LocalDate dateSort;
	private int prixRes;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client_) {
		client = client_;
	}
	
	public String getNumReservation() {
		return numReservation;
	}
	public void getNumReservation(String numReservation_) {
		numReservation = numReservation_;
	}

	public LocalDate getDateEnt() {
		return dateEnt;
	}

	public void setDateEnt(LocalDate dateEnt_) {
		dateEnt = dateEnt_;
	}

	public LocalDate getDateSort() {
		return dateSort;
	}

	public void setDateSort(LocalDate dateSort_) {
		dateSort = dateSort_;
	}

	public int getPrixRes() {
		return prixRes;
	}

	public void setPrixRes(int prix_) {
		prixRes = prix_;
	}

	public Reservation(Scanner in) {
		System.out.println("Entrez le numéro de reservation");
		String numReserv = in.nextLine();
		numReservation = numReserv;
		client = new Client();
		date(in);
	}

	public Reservation(Client client_, Scanner in) {	
		System.out.println("Entrez le numéro de reservation");
		String numReserv = in.nextLine();
		numReservation = numReserv;
		client = client_;
		date(in);
		
		
	}
	/**
	 * Fonction qui verifie la validitée des dates d'entrer et de sortie,
	 * @param in	la saisie des dates
	 */
	public void date(Scanner in) {
		boolean verifDateEntree = false;
		while (!verifDateEntree) {
			verifDateEntree = true;
			System.out.println("Entrer la date de debut de votre sejour");
			String dateDebut = in.nextLine();
			String[] d1 = dateDebut.split("/");
			try {
				LocalDate date1 = LocalDate.parse(d1[2] + "-" + d1[1] + "-" + d1[0]);
				dateEnt = date1;

			} catch (DateTimeParseException ex) {
				System.out.println("la date n'est pas valide");
				verifDateEntree = false;
			}
		}
		boolean verifDateSorti = true;
		while (verifDateSorti) {
			verifDateSorti = false;
			System.out.println("Entrer la date de fin de votre sejour");
			String dateFin = in.nextLine();
			String[] d2 = dateFin.split("/");
			try {
				LocalDate date2 = LocalDate.parse(d2[2] + "-" + d2[1] + "-" + d2[0]);
				dateSort = date2;
				if (dateSort.isBefore(dateEnt)) {
					verifDateSorti = true;
				}
			} catch (DateTimeParseException ex) {
				System.out.println("la date n'est pas valide");
			}
		}
	}
	/**
	 * Fonction qui nous retourne le temps d'occupation du sejour
	 * @return le temps d'occupation
	 */
	public int duree () {
		int duree = Period.between(dateEnt,dateSort ).getDays();
		return duree;
	}
	
	
	public void remboursement(int tarif) {
		System.out.println("On vous rembourse la somme de "+ tarif(tarif)+" euros");
	}
	
	/**
	 * affiche le detail de la reservation
	 */
	
	 public void detailReservation(Chambre chambre) {
	 System.out.println("-------------HOTEL CALIFORNIA--------------");
	 System.out.println("Votre reservations :");
	 System.out.println("Nom : "+client.getNom());
	 System.out.println("Prenom : "+client.getPrenom());
	 System.out.println("Email : "+client.getEMail());
	 System.out.println("Vous avez reserver la chambre numero : " + chambre.getNumChambre()	+"\n" +
				 chambre.getType() + "\n pour la periode du "+
				 dateEnt +" au "+dateSort +"\n Numero de reservation : "+numReservation); 
	 System.out.println("Prix du sejour :"+tarif(chambre.getTarif())+" Euros"); 
	 		
	 }
	 /**
		 * Fonction qui calcul le prix du sejour
		 * @return 	le prix du sejour
		 */
		public int tarif (int tarif) {
			int total=duree()*tarif;
			return total;
		}
	/**
	 * Fonction qui demande le numero de carte bancaire du client et genere le PDF
	 * @param chambre	la chambre reserve
	 * @param in	on attend le numero de la carte bancaire
	 */
	 public void payer(Chambre chambre,Scanner in) {
	 System.out.println("Vous devez payer :"+tarif(chambre.getTarif()));
	 System.out.println("Veuillez rentrer le numero de votre carte "); 
	 int dollar= in.nextInt(); 
	 in.nextLine(); 
	 System.out.println("Merci !"); 
	 creerPDF(chambre);
	 //envoiMail();
	 }
	 

	/**
	 * generation de la reservation en pdf
	 */
	
	public void creerPDF(Chambre chambre) { 
	String chemin =	"C:\\ENV\\Ressources\\reservation.pdf"; 
	try { 
	PdfWriter writer = new PdfWriter(chemin); 
	PdfDocument pdf = new PdfDocument(writer);
	Document document = new Document(pdf);
	document.add(new Paragraph("Votre reservation :"));

	document.add(new Paragraph("-------------HOTEL CALIFORNIA--------------"));
	document.add(new Paragraph("Votre reservations :"));
	document.add(new Paragraph("Nom : "+client.getNom()));
	document.add(new Paragraph("Prenom : "+client.getPrenom()));
	document.add(new Paragraph("Email : "+client.getEMail()));
	document.add(new Paragraph("Vous avez reserver la chambre numero : " + chambre.getNumChambre()	+"\n" +
				 chambre.getType() + "\n pour la periode du "+
				 dateEnt +" au "+dateSort)); 
	document.add(new Paragraph("Prix du sejour :"+tarif(chambre.getTarif())+" Euros")); 
	
	document.close();
	} catch (FileNotFoundException e) { 
	e.printStackTrace(); } }
	 
	 /** envoie du mail avec la reservation en pdf en piece jointe
	/
	/*
	 * public void envoiMail() { String user = "fayak.akbaraly@gmail.com"; String
	 * pass = "luffy974"; String serveur = "smtp.gmail.com";
	 * 
	 * Properties props = System.getProperties(); props.put("mail.smtp.host",
	 * serveur); props.put("mail.smtp.starttls.enable", "true");
	 * props.put("mail.smtp.port", 587); props.put("mail.smtp.auth", "true");
	 * Session session = Session.getInstance(props);
	 * 
	 * Message message = new MimeMessage(session); try { message.setFrom(new
	 * InternetAddress(user)); InternetAddress[] internetAddresses = new
	 * InternetAddress[1]; internetAddresses[0] = new
	 * InternetAddress(client.getEMail());
	 * message.setRecipients(Message.RecipientType.TO, internetAddresses);
	 * message.setSubject("Reservation HOTEL CALIFORNIA");
	 * message.setText("Mail avec une piece jointe"); MimeBodyPart body = new
	 * MimeBodyPart(); body.
	 * setText("Bonjour, Vous retrouverez ci-joint le detail de votre reservation de chambre\nCordialement,\nL'equipe CALIFORNIA"
	 * ); MimeBodyPart attachMent1 = new MimeBodyPart(); String fichier =
	 * "C:\\ENV\\Ressources\\reservation.pdf";
	 * attachMent1.attachFile(fichier); Multipart multipart = new MimeMultipart();
	 * multipart.addBodyPart(body); multipart.addBodyPart(attachMent1);
	 * message.setContent(multipart);
	 * 
	 * Transport transport = session.getTransport("smtp");
	 * transport.connect(serveur, user, pass); transport.sendMessage(message,
	 * internetAddresses); transport.close();
	 * 
	 * } catch (MessagingException | IOException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * 
	 * }
	 */

}
