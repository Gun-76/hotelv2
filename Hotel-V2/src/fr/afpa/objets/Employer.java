package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Employer {

	private String nom;
	private String login;
	private String passWord;
	
	public String getNom () {
		return nom;
	}
	public void setNom(String nom_){
		nom=nom_;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login_) {
		login=login_;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord_) {
		passWord=passWord_;
	}
	/**
	 * cr�ation de l'employer � l'aide du constructeur
	 */
	public Employer() {
		Scanner in = new Scanner(System.in);
		System.out.println("Cretion de l'employe :");
		System.out.println("Saisir le nom de l'employe");
		String nom_ = in.nextLine();
		System.out.println("Saisir l'identifiant : GH puis 3 chiffres ");
		String login_ = in.nextLine();
		System.out.println("Saisir le mot de passe de l'employe");
		String passWord_ = in.nextLine();
		
		if(isValidLogEmployer(login_)) {
		try {
			FileWriter fw = new FileWriter("C:\\ENV\\Ressources\\employer.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(nom_);
			nom = nom_;
			bw.newLine();
			bw.write(login);
			bw.newLine();
			bw.write(passWord_);
			passWord = passWord_;
			bw.close();			
		}
		catch(Exception e) {
			System.out.println("Erreur "+e);
		}
		}
	}
	@Override
	public String toString() {
		return "Employer [nom=" + nom + ", login=" + login + ", passWord=" + passWord + "]";
	}
	/**
	 * Fonction pour authentifier l'employer
	 * @return true si le nom correspond, sinon false
	 */
	public boolean seConnecter() {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez l'identifiant");
		String login_ = in.nextLine();
		System.out.println("Entrez le mot de passe");
		String passWord_ = in.nextLine();
		String[] tabLogEmployer = new String[3];
		int i = 0;
		try {
			FileReader fr = new FileReader("C:\\ENV\\Ressources\\employer.txt");
			BufferedReader br = new BufferedReader(fr);
			while(br.ready()) {
				tabLogEmployer[i] = br.readLine();
				i++;
			}
			br.close();
		}
		catch(Exception e) {
			System.out.println("Erreur "+e);
		}
		
		if(login_.equals(tabLogEmployer[1]) && passWord_.equals(tabLogEmployer[2])) {
			System.out.println("Bienvenue "+nom);
			return true;
		}
		else {
			System.out.println("Login ou mot de passe incorrect");
		}
		return false;
	}
	/**
	 * Verification des contrainte de login pour l'employer
	 * @param login_	
	 * @return	retourne un boolean pour validation
	 */
	public boolean isValidLogEmployer(String login_) {
		boolean isValid = false;
		Scanner in = new Scanner(System.in);
		while(!isValid || login_.length() != 5) {
			if(login_.length() == 5) {
				for(int i=2;i<login_.length();i++) {
					if(login_.charAt(0) == 'G' && login_.charAt(1)=='H') {
						isValid=true;
					}
					else {
						isValid = false;
						System.out.println("Veuillez saisir GH au debut de votre identifiant");
						login_=in.nextLine();
						break;
					}
					if(Character.isDigit(login_.charAt(i))) {
						isValid = true;
					}
					else {
						isValid = false;
						System.out.println("Veuillez saisir 3 chiffres apres GH pour l'identifiant de l'employer");
						login_=in.nextLine();
						break;
					}
				}
			}else {
				System.out.println("veuillez saisir 5 caractere pour l'identifiant de l'employer");
				login_=in.nextLine();
			}
			
		}
		if(isValid) {
			setLogin(login_);			
		}
		return isValid;
	}
}
