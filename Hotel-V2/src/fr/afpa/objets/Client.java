package fr.afpa.objets;

import java.util.Scanner;

public class Client {

	private String nom;
	private String prenom;
	private String login;
	private String eMail;
	
	public String getNom () {
		return nom;
	}
	public void setNom(String nom_) {
		nom = nom_;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom_) {
		prenom=prenom_;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login_) {
		login = login_;
	}
	public String getEMail() {
		return eMail;
	}
	public void setEMail(String eMail_) {
		eMail=eMail_;
	}
	/**
	 * fonction qui cr�e un client	
	 */
	public Client() {
		Scanner in = new Scanner(System.in);
		System.out.println("Saisir le nom de du client");
		String nom_ = in.nextLine();
		System.out.println("Saisir le prenom du client");
		String prenom_ = in.nextLine();
		System.out.println("Saisir l'identifiant du client");
		String login_ = in.nextLine();
		System.out.println("Saisir le mot de passe du client");
		String passWord_ = in.nextLine();
		System.out.println("Saisir l'adresse email du client");
		String eMail_ = in.nextLine();
		
		if(validLogin(login_)) {
		try {			
			nom = nom_;			
			prenom = prenom_;			
			login = login_;					
			eMail = eMail_;			
		}
		catch(Exception e) {
			System.out.println("Erreur "+e);
		}
	  }
	}
	
	
	
	
	/**
	 * Fonction pour authentifier le client
	 * @return true si le nom correspond, sinon false
	 */
	public boolean seConnecter(String login_) {
		Scanner in = new Scanner(System.in);
		System.out.println("Entrez votre login :");
		login_ = in.nextLine();
		if(login_.equals(login)) {
			System.out.println("Bienvenue "+nom);
			return true;
		}
		else {
			System.out.println("Login incorrect ou client pas cree");
		}
		return false;
	}
	
	
	/**
	 * fonction qui verifie si le login du client contient 10 chiffres
	 * @param login_
	 * @return true si il est valide et false si invalide;
	 */
	public boolean validLogin(String login_) {
		boolean isValid = false;
		Scanner in = new Scanner(System.in);
		while(!isValid || login_.length() != 10) {
			if(login_.length() == 10) {
				for(int i=0;i<login_.length();i++) {
					if(Character.isDigit(login_.charAt(i))) {
						isValid = true;
					}
					else {
						isValid = false;
						System.out.println("Veuillez entrez 10 chiffres pour l'identifiant du client");
						login_= in.nextLine();
						break;
					}
				}
			}
			else {
				System.out.println("veuillez saisir 10 caracteres pour l'identifiant du client");
				login_= in.nextLine();
			}
			
		}
		if(isValid) {
			setLogin(login_);
		}
		return isValid;
	}
	
	@Override
	public String toString() {
		return "Client [nom=" + nom + ", prenom=" + prenom + ", login=" + login + ", eMail="
				+ eMail + "]";
	}
	
}
